import { GraphqlQueryError } from "@shopify/shopify-api";
import shopify from "./shopify.js";

const ADJECTIVES = [
  "autumn",
  "hidden",
  "bitter",
  "misty",
  "silent",
  "empty",
  "dry",
  "dark",
  "summer",
  "icy",
  "delicate",
  "quiet",
  "white",
  "cool",
  "spring",
  "winter",
  "patient",
  "twilight",
  "dawn",
  "crimson",
  "wispy",
  "weathered",
  "blue",
  "billowing",
  "broken",
  "cold",
  "damp",
  "falling",
  "frosty",
  "green",
  "long",
];

const NOUNS = [
  "waterfall",
  "river",
  "breeze",
  "moon",
  "rain",
  "wind",
  "sea",
  "morning",
  "snow",
  "lake",
  "sunset",
  "pine",
  "shadow",
  "leaf",
  "dawn",
  "glitter",
  "forest",
  "hill",
  "cloud",
  "meadow",
  "sun",
  "glade",
  "bird",
  "brook",
  "butterfly",
  "bush",
  "dew",
  "dust",
  "field",
  "fire",
  "flower",
];

export const DEFAULT_PRODUCTS_COUNT = 1;
const CREATE_PRODUCTS_MUTATION = `
  mutation populateProduct($input: ProductInput!) {
    productCreate(input: $input) {
      product {
        id
      }
    }
  }
`;


//use graphql to create the product

// export default async function productCreator(
//   session,
//   product = {}
// ) {

//   const client = new shopify.api.clients.Graphql({ session });

//   try {

//       await client.query({
//         data: {
//           query: CREATE_PRODUCTS_MUTATION,
//           variables: {
//             input: {
//               title: `${product.title}`,
//               variants: [{ price: product.price }],
//             },
//           },
//         },
//       });
    
//   } catch (error) {
//     if (error instanceof GraphqlQueryError) {
//       throw new Error(
//         `${error.message}\n${JSON.stringify(error.response, null, 2)}`
//       );
//     } else {
//       throw error;
//     }
//   }
// }

//use restapi to create the product

import axios from 'axios';

export default async function productCreator(session, product = {}) {
  const { shop, accessToken } = session;

  try {
    const response = await axios.post(
      `https://${shop}/admin/api/2023-04/products.json`,
      {
        product: {
          title: product.title ,
          body_html: product.description ,
          images: [
            {
              src: product.imageUrl ,
            },
          ],
          tags: product.tags  ,
          variants: [
            {
              price: product.price 
            },
          ],
        },
      },
      {
        headers: {
          'Content-Type': 'application/json',
          'X-Shopify-Access-Token': accessToken,
        },
      }
    );

    console.log('Product created successfully:', response.data.product);
  } catch (error) {
    console.error('Error creating product:', error.message);
    if (error.response) {
      console.error('Response data:', error.response.data);
    }
    throw error;
  }
}


function randomTitle() {
  const adjective = ADJECTIVES[Math.floor(Math.random() * ADJECTIVES.length)];
  const noun = NOUNS[Math.floor(Math.random() * NOUNS.length)];
  return `${adjective} ${noun}`;
}

function randomPrice() {
  return Math.round((Math.random() * 10 + Number.EPSILON) * 100) / 100;
}
