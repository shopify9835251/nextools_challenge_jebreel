// @ts-check
import { join } from "path";
import { readFileSync } from "fs";
import express from "express";
import serveStatic from "serve-static";

import shopify from "./shopify.js";
import productCreator from "./product-creator.js";
import PrivacyWebhookHandlers from "./privacy.js";

// Import required modules
import session from 'express-session';
import SQLiteStoreFactory from 'connect-sqlite3';
//import { SQLiteSessionStorage } from "@shopify/shopify-app-session-storage-sqlite";

// Create an SQLite store
const SQLiteStore = SQLiteStoreFactory(session);

const PORT = parseInt(
  process.env.BACKEND_PORT || process.env.PORT || "3000",
  10
);

const STATIC_PATH =
  process.env.NODE_ENV === "production"
    ? `${process.cwd()}/frontend/dist`
    : `${process.cwd()}/frontend/`;

const app = express();

//implement session storage using sqlite database 
app.use(
  session({
    secret: 'your-session-secret',
    resave: false,
    saveUninitialized: true,
  })
);

// Set up Shopify authentication and webhook handling
app.get(shopify.config.auth.path, shopify.auth.begin());
app.get(
  shopify.config.auth.callbackPath,
  shopify.auth.callback(),
  shopify.redirectToShopifyOrAppRoot()
);
app.post(
  shopify.config.webhooks.path,
  shopify.processWebhooks({ webhookHandlers: PrivacyWebhookHandlers })
);

// If you are adding routes outside of the /api path, remember to
// also add a proxy rule for them in web/frontend/vite.config.js

app.use("/api/*", shopify.validateAuthenticatedSession());

app.use(express.json());

// GETTING STORE INFORMATION
app.get("/api/store/info", async(req, res) => {
  let storeInfo = await shopify.api.rest.Shop.all({
    session: res.locals.shopify.session,
  });
  res.status(200).send(storeInfo);
});

// GETTING STORE ORDERS
app.get("/api/orders/all", async(req, res) => {
  let storeOrders = await shopify.api.rest.Order.all({
    session: res.locals.shopify.session,
    status: "any",
  });
  res.status(200).send(storeOrders);
});


// READ ALL PRODUCTS
app.get("/api/product/count", async(req, res) => {
  let totalProducts = await shopify.api.rest.Product.count({
    session: res.locals.shopify.session,
  });
  res.status(200).send(totalProducts);
});
// Get the total totoal product
app.get("/api/products/count", async (_req, res) => {
  const countData = await shopify.api.rest.Product.count({
    session: res.locals.shopify.session,
  });
  res.status(200).send(countData);
});

// READ ALL COLLECTIONS
app.get("/api/collection/count", async(req, res) => {
  let totalCollections = await shopify.api.rest.CustomCollection.all({
    session: res.locals.shopify.session,
  });
  res.status(200).send(totalCollections);
})

// Add new product

app.get("/api/products/create", async (req, res) => {
  let status = 200;
  let error = null;
  const formData = req.query;
  console.log("api/products/create",formData)
  try {
    await productCreator(res.locals.shopify.session, formData);
  } catch (e) {
    console.log(`Failed to process products/create: ${e.message}`);
    status = 500;
    error = e.message;
  }
  res.status(status).send({ success: status === 200, error });
});


app.get("/api/products", async (req, res) => {

  try {
    
    // create a session
    const session = res.locals.shopify.session
    // create client
    const data = await shopify.api.rest.Product.all({session:session})
    res.status(200).send(data)
  } catch (err) {
    console.error(err);
  }
});

//graphql for fetching orders

app.get("/api/orders", async (req, res) => {
  try {
    
    // create a session
    const session = res.locals.shopify.session
    // create client
    const client = new shopify.api.clients.Graphql({session})

    const queryString = `{
      orders(first:5){
        edges{

        }node {
          id
          name
          note
          displayFinancialStatus
        }
      }
    }`
  
    const data = await client.query({
      data: queryString
    });

    res.status(200).send({data})
  } catch (err) {
    res.status(500).send(err );
  }

  
});

//graphql for fetching collections

app.get("/api/collections", async (req, res) => {
  try {
    
    // create a session
    const session = res.locals.shopify.session
    // create client
    const client = new shopify.api.clients.Graphql({session})
    const response = await client.query({
      data:`query{
        collections(first:5){
          edges{
          node {
            id
            title
            handle
            updateAt
            productsCount
            sorttOrder
          }
        }
      }
      }`
    })
    res.status(200).send(response);
  } catch (err) {
    res.status(500).send(err );
  }

  
});

app.use(shopify.cspHeaders());
app.use(serveStatic(STATIC_PATH, { index: false }));

app.use("/*", shopify.ensureInstalledOnShop(), async (_req, res, _next) => {
  return res
    .status(200)
    .set("Content-Type", "text/html")
    .send(readFileSync(join(STATIC_PATH, "index.html")));
});

app.listen(PORT);