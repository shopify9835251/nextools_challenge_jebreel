import sqlite3 from 'sqlite3';
import { SQLiteSessionStorage } from "@shopify/shopify-app-session-storage-sqlite";
import { LATEST_API_VERSION } from "@shopify/shopify-api";
import { shopifyApp } from "@shopify/shopify-app-express";
import { restResources } from "@shopify/shopify-api/rest/admin/2023-04";

const DB_PATH = `${process.cwd()}/database.sqlite`;
const connection = new sqlite3.Database(DB_PATH);

async function storecallback(session) {
    try {
        let data = session;
        data.onlineAccessInfo = JSON.stringify(session.onlineAccessInfo);

        let domain_id = null;

        if (data.id.indexOf(`${data.shop}`) > -1) {
            domain_id = data.id;
        }

        connection.run(`
            INSERT OR REPLACE INTO shop(shop_url, session_id, domain_id, accessToken, state, isOnline, onlineAccessInfo, scope)
            VALUES ('${data.shop}', '${data.id}', '${domain_id}', '${data.accessToken}', '${data.state}', '${data.isOnline}', '${data.onlineAccessInfo}', '${data.scope}')
        `, [data.shop, data.id, domain_id, data.accessToken, data.state, data.isOnline, data.onlineAccessInfo, data.scope], (error) => {
            if (error) throw error;
        });

        return true;

    } catch (e) {
        throw new Error(e);
    }
}

async function loadcallback(id) {
    try {
        let session = new Session(id);

        let query = new Promise((resolve, reject) => {
            connection.get(`
                SELECT * FROM shop WHERE session_id=? OR domain_id=? LIMIT 1
            `, [id, id], (error, result) => {
                if (error) throw error;
                if (result) {
                    session.shop = result.shop_url;
                    session.state = result.state;
                    session.scope = result.scope;
                    session.isOnline = result.isOnline === 'true';
                    session.onlineAccessInfo = result.onlineAccessInfo;
                    session.accessToken = result.accessToken;

                    const date = new Date();
                    date.setDate(date.getDate() + 1);
                    session.expires = date;

                    if (session.expires && typeof session.expires === 'string') {
                        session.expires = new Date(session.expires);
                    }
                }
                resolve();
            });
        });

        await query;
        return session;

    } catch (err) {
        throw new Error(err);
    }
}

async function deletecallback(id) {
    try {
        return false;

    } catch (e) {
        throw new Error(e);
    }
}

const shopify = shopifyApp({
  api: {
    apiVersion: LATEST_API_VERSION,
    restResources,
    billing: undefined,
  },
  auth: {
    path: "/api/auth",
    callbackPath: "/api/auth/callback",
  },
  webhooks: {
    path: "/api/webhooks",
  },
  sessionStorage: new SQLiteSessionStorage(DB_PATH),
});

export {
    storecallback,
    loadcallback,
    deletecallback,
   
};
export default shopify;