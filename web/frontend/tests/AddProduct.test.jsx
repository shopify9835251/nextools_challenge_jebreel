import { render, fireEvent, act, waitFor } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import AddProduct from '../pages/AddProduct';

test('should update title field in form data on title change', async () => {
  const { getByTestId } = render(<AddProduct />);
  const titleInput = getByTestId('title');

  // Use act to handle asynchronous updates
  await act(async () => {
    fireEvent.change(titleInput, { target: { value: 'new title' } });

    // Wait for React to update
    await waitFor(() => {
      // Ensure that the titleInput value matches the expected value
      expect(titleInput.value).toBe('new title');
    });
  });
});
