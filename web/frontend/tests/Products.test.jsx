import React from 'react';
import { render, waitFor, screen } from '@testing-library/react'; 
import '@testing-library/jest-dom';
import Products from '../pages/Products';

// Mock the fetch function
global.fetch = jest.fn(() =>
  Promise.resolve({
    json: () => Promise.resolve({ data: [{ id: 1, name: 'Product 1' }] }),
  })
);

describe('Products', () => {
  it('fetches and displays products', async () => {
    render(<Products/>);

    // Wait for the component to render after the data fetch
    await waitFor(() => {
      expect(global.fetch).toHaveBeenCalledWith('/api/products');
      expect(screen.getByText('Product 1')).toBeInTheDocument();
    });
  });
});
