import { Layout, LegacyCard } from '@shopify/polaris';
import React from 'react';

export function Card({ title, orders, total, fulfilled, remains, productcount, collectioncount }) {
  // Check if orders is not null or undefined, and if data property exists
  let completed = orders?.data?.filter(item => item.fulfillment_status === 'fulfilled') || [];

  return (
    <>
      <Layout.Section oneThird>
        <LegacyCard title={title} sectioned>
          <h1 className='total_count'>
            {total ? (orders && orders.data ? orders.data.length : '') : ''}
            {fulfilled ? (completed ? completed.length : '') : ''}
            {remains ? (completed ? orders.data.length - completed.length : '') : ''}
            {productcount ? (orders ? orders.count : '') : ''}
            {collectioncount ? (orders && orders.data ? orders.data.length : '') : ''}
          </h1>
        </LegacyCard>
      </Layout.Section>
    </>
  );
}
