import { useState , useEffect} from "react";
import {
  TextField,
  Button,
  Grid,
  Paper,
  Typography,
} from '@mui/material';
import {
  Card,
  Heading,
  TextContainer,
  DisplayText,
  TextStyle,
} from "@shopify/polaris";
import { Toast } from "@shopify/app-bridge-react";
import { useAppQuery, useAuthenticatedFetch } from "../hooks";


function Addproduct() {
  const emptyToastProps = { content: null };
  const [isLoading, setIsLoading] = useState(true);
  const [toastProps, setToastProps] = useState(emptyToastProps);
  const fetch = useAuthenticatedFetch();
  const [formData, setFormData] = useState({
    title: '',
    description: '',
    image: '',
    tag: '',
    metafields: [{ key: '', value: '' }],
  });

  const {
    data,
    refetch: refetchProductCount,
    isLoading: isLoadingCount,
    isRefetching: isRefetchingCount,
  } = useAppQuery({
    url: "/api/products/count",
    reactQueryOptions: {
      onSuccess: () => {
        setIsLoading(false);
      },
    },
  });

  const toastMarkup = toastProps.content && !isRefetchingCount && (
    <Toast {...toastProps} onDismiss={() => setToastProps(emptyToastProps)} />
  );

  useEffect(() => {
    // Fetch data from SQLite session storage when the component mounts
    const storedFormData = sessionStorage.getItem("formData");
    if (storedFormData) {
      setFormData(JSON.parse(storedFormData));
    }
  }, []);

  const handlePopulate = async (e) => {
    setIsLoading(true);
    e.preventDefault();
    const queryParams = new URLSearchParams(formData).toString();
    console.log('handlePopulate Form submitted:', queryParams);
    

    const url = `/api/products/create?${queryParams}`;
    
    const response = await fetch(url);
    if (response.ok) {
      await refetchProductCount();
      setToastProps({ content: "your product was added successfuly to the store!" });
      sessionStorage.removeItem("formData");
    } else {
      setIsLoading(false);
      setToastProps({
        content: "There was an error creating products",
        error: true,
      });
    }
  };
  

  const handleFormChange = (field, value) => {
    setFormData({ ...formData, [field]: value });
  };

  const handleMetafieldChange = (index, field, value) => {
    const newMetafields = [...formData.metafields];
    newMetafields[index][field] = value;
    setFormData({ ...formData, metafields: newMetafields });
    console.log(storedFormData,'stored data');
    // Save updated form data to sessionStorage
    const updatedFormData = { ...formData, metafields: newMetafields };
    sessionStorage.setItem("formData", JSON.stringify(updatedFormData));
  };
  
  
  const addMetafield = () => {
    setFormData({
      ...formData,
      metafields: [...formData.metafields, { key: '', value: '' }],
    });
  };


  return (
    <>
      {toastMarkup}
      <Grid container justifyContent="center" alignItems="center" style={{ height: '100vh' }}>
      <Grid item xs={10} sm={8} md={6} lg={4}>
        <Paper elevation={3} style={{ padding: '20px' }}>
          <Typography variant="h5" align="center" gutterBottom>
            Add a New Product
          </Typography>

          <form onSubmit={handlePopulate}>
          <TextField
            label="Title"
            fullWidth
            margin="normal"
            value={formData.title}
            onChange={(e) => handleFormChange('title', e.target.value)}
            inputProps={{ "data-testid": "title-input" }}  
          />


            <TextField
              label="Description"
              fullWidth
              multiline
              rows={4}
              margin="normal"
              value={formData.description}
              onChange={(e) => handleFormChange('description', e.target.value)}
            />

            <TextField
              label="Image URL"
              fullWidth
              margin="normal"
              value={formData.image}
              onChange={(e) => handleFormChange('image', e.target.value)}
            />

            <TextField
              label="Tag"
              fullWidth
              margin="normal"
              value={formData.tag}
              onChange={(e) => handleFormChange('tag', e.target.value)}
            />

            {formData.metafields.map((metafield, index) => (
              <div key={index}>
                <TextField
                  label={`Metafield Key ${index + 1}`}
                  fullWidth
                  margin="normal"
                  value={metafield.key}
                  onChange={(e) => handleMetafieldChange(index, 'key', e.target.value)}
                />

                <TextField
                  label={`Metafield Value ${index + 1}`}
                  fullWidth
                  margin="normal"
                  value={metafield.value}
                  onChange={(e) => handleMetafieldChange(index, 'value', e.target.value)}
                />
              </div>
            ))}

            <Button
              type="button"
              variant="outlined"
              color="primary"
              fullWidth
              style={{ marginTop: '10px' }}
              onClick={addMetafield}
            >
              Add Metafield
            </Button>

            <Button
              type="submit"
              variant="contained"
              color="primary"
              fullWidth
              style={{ marginTop: '20px' }}
            >
              Save
            </Button>
          </form>
        </Paper>
      </Grid>
    </Grid>
    </>
  );
}

export default Addproduct;