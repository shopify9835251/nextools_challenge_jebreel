import { useState, useCallback, useEffect } from "react";
import { useAuthenticatedFetch, useAppQuery } from "../hooks";

import {
  LegacyCard,
  Layout,
  Page,
  ResourceList,
  Select,
  LegacyStack,
  Stack,
  TextField,
  TextStyle,
} from "@shopify/polaris";

const Products = () => {

const fetch = useAuthenticatedFetch();
const [products, setProducts] = useState([]);

useEffect(() => {
  const fetchData = async () => {
    try {
      const response = await fetch("/api/products");
      const data = await response.json();
      setProducts(data.data);
      console.log(data.data)
    } catch (err) {
      console.log(err);
    }
  };
  fetchData();
}, []);

    return (
      <Page>
        <Layout>
          <Layout.Section>
            <LegacyCard>
              <ResourceList
                resourceName={{ singular: "product", plural: "products" }}
                items={products}
                renderItem={(item) => {
                  const { title, id, vendor } = item;
                  return (
                    <ResourceList.Item
                      id={id}
                      accessibilityLabel={`View details for ${title}`}
                    >
                      <LegacyStack>
                        <Stack.Item fill>
                          <h3>
                            <TextStyle variation="strong">{title}</TextStyle>
                          </h3>
                        </Stack.Item>
                        <Stack.Item>
                          <p>{vendor}</p>
                        </Stack.Item>
                      </LegacyStack>
                    </ResourceList.Item>
                  );
                }}
              />
            </LegacyCard>
          </Layout.Section>
        </Layout>
      </Page>
    );
  };
export default Products

