FROM node:18-alpine

# Set environment variables
ENV SHOPIFY_API_KEY=1244b1192d9025302b9c10e01234e957
ENV SHOPIFY_SECRET_KEY=ba3b2f62b1f6e374adf8e4b294881a5b
ENV SCOPES=write_products,read_products,read_orders
ENV HOST=127.0.0.1
ENV PORT=8081

# Set working directory
WORKDIR /app

# Copy the contents of the 'web' directory into the container
COPY web .

# Install dependencies
RUN npm install

# Set the Shopify API key specifically for the frontend build
WORKDIR /app/frontend
RUN yarn install

# Reset working directory
WORKDIR /app

# Copy the docker-entrypoint.sh script
COPY docker-entrypoint.sh /app

# Grant execute permission to the script
RUN chmod +x /app/docker-entrypoint.sh

# Set the default command to run your application
CMD ["npm", "run", "dev"]
