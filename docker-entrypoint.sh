#!/bin/bash

# Grant necessary permissions or run Docker-related commands here

# For example, run a Docker command with elevated privileges
docker run -it --rm shopify:task/app

# Your other startup commands go here
exec "$@"
